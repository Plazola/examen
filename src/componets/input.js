import React from 'react';
import './input.css';
import 'bootstrap/dist/css/bootstrap.css';
function Input(props) {

    return (
        <div className="form-group">
            <label htmlFor={props.id}>{props.label}</label>
            <input className="Input form-control" type="text" id={props.id} placeholder={props.placeholder}>      
            </input>
            
        </div>
    )
}

export default Input;