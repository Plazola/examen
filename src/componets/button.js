import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import $ from 'jquery';
function Button(props) {
    function handleClick() {
        console.log($('#fecha-entrada').val());
        
        var date = new Date($('#fecha-entrada').val());
        
        if($('#fecha-entrada').val()==''){
            var errormsg = document.getElementById("access-code-error");
            errormsg.innerHTML = "No se escribio ningun dato";
        }else if(isNaN(date.getTime())){
            var errormsg = document.getElementById("access-code-error");
            errormsg.innerHTML = "Error de formato";
        }else{            
            var month = "0"+(date.getMonth()+1);
            var day = "0"+date.getDate();
            month = month.slice(-2);
            day = day.slice(-2);
            const dateString = date.getFullYear()+''+month+''+day;
            $('#fecha-salida').val(dateString)
        }
    }
    return (
        <div className="form-group">       
            <button type="submit" className="btn btn-primary" onClick={handleClick.bind(this)}>{props.titulo}</button>
        </div>
    )
}

export default Button;