import React from 'react';
import ReactDOM from 'react-dom';
import logo from './logo.svg';
import Input from './componets/input';
import './App.css';
import Button from './componets/button';


const app = document.getElementById('app')

function App() {
  return (
    <div className="App">
      <header className="App-header">       
        <div>
        {<Input id="fecha-entrada" placeholder="M/D/YYYY" label="Fecha de entrada" />}
        {<Input id="fecha-salida" placeholder="YYYYMMDD" label="Fecha de salida" />}
        <span id="access-code-error" className="rsvp required-fields"> </span>
        <br></br>
        {<Button titulo="Convertir"/>}
        
        
        </div>        
      </header>
    </div>
  );
}

export default App;
